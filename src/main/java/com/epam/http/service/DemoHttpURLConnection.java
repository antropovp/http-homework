package com.epam.http.service;



import javax.xml.ws.http.HTTPException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DemoHttpURLConnection implements DemoService {
    String query;
    String method;
    String version = "1.1";
    String contentType = "text/plain; charset=utf-8";
    String body = "Some text";
    boolean doutput = true;
    boolean usecaches = false;

    HttpURLConnection connection = null;

    public void demo() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

            System.out.print("URI: ");
            query = br.readLine();
            System.out.print("METHOD: ");
            method = br.readLine().toUpperCase();

            System.out.println("Opening the connection...");
            connection = (HttpURLConnection) new URL(query).openConnection();

            request();

            if (HttpURLConnection.HTTP_OK == connection.getResponseCode()) {
                br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                while (br.ready()) {
                    System.out.println(br.readLine());
                }
            } else {
                System.out.println("Error " + connection.getResponseCode() + "\n" + connection.getResponseMessage());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (HTTPException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
    private void request() throws IOException {
        System.out.println("Sending the request...");

        connection.setRequestMethod(method);
        connection.setRequestProperty("Version", version);
        connection.setRequestProperty("Content-Type", contentType);
        connection.setUseCaches(usecaches);
        connection.setDoOutput(doutput);

        connection.connect();

        if (method.equals("GET")||method.equals("HEAD")||method.equals("OPTIONS")||method.equals("DELETE")) {

        }
        else if (method.equals("POST")||method.equals("PUT")||method.equals("PATCH")) {
            OutputStream os = connection.getOutputStream();
            os.write(body.getBytes());
            os.flush();
            os.close();
        }
        else {
            System.out.println("Error: Unknown request.");
        }
    }
}
