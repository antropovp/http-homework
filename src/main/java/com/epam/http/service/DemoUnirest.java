package com.epam.http.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.GetRequest;
import com.mashape.unirest.request.HttpRequestWithBody;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class DemoUnirest implements DemoService {
    String query;
    String method;
    String accept = "text/plain";
    String acceptCharset = "utf-8";
    String body = "Some text";

    HttpResponse<String> response;

    public void demo() {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));


            System.out.print("URI: ");
            query = br.readLine();
            System.out.print("METHOD: ");
            method = br.readLine().toUpperCase();

            this.request(method);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnirestException e) {
            e.printStackTrace();
        } finally {
            try {
                Unirest.shutdown();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    private void request(String method) throws UnirestException {
        if (method.equals("GET")) { GET(); }
        else if (method.equals("HEAD")) { HEAD(); }
        else if (method.equals("POST")) { POST(); }
        else if (method.equals("PUT")) { PUT(); }
        else if (method.equals("PATCH")) { PATCH(); }
        else if (method.equals("OPTIONS")) { OPTIONS(); }
        else if (method.equals("DELETE")) { DELETE(); }
        else {
            System.out.println("Error: Unknown request.");
        }

        System.out.println(response.getBody());
    }

    private void GET() throws UnirestException {
        response = Unirest.get(query).asString();
    }
    private void HEAD() throws UnirestException {
        response = Unirest.head(query).asString();
    }
    private void POST() throws UnirestException {
        response = Unirest.post(query)
                .field("username", "elon-musk")
                .field("password", "falcon9")
                .asString();
    }
    private void PUT() throws UnirestException {
        response = Unirest.put(query)
                .header("Accept", accept)
                .header("Accept-Charset", acceptCharset)
                .body(body)
                .asString();
    }
    private void PATCH() throws UnirestException {
        response = Unirest.patch(query)
                .header("Accept", accept)
                .header("Accept-Charset", acceptCharset)
                .body(body)
                .asString();
    }
    private void OPTIONS() throws UnirestException {
        response = Unirest.options(query)
                .asString();
    }
    private void DELETE() throws UnirestException {
        response = Unirest.delete(query)
                .asString();
    }
}
