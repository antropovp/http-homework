package com.epam.http;

import com.epam.http.service.DemoHttpURLConnection;
import com.epam.http.service.DemoService;
import com.epam.http.service.DemoUnirest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Main {

    public static void main(String args[]) {
        DemoService demoService;

        System.out.println("###DEMO HttpURLConnection###");
        demoService = new DemoHttpURLConnection();
        demoService.demo();
        System.out.println("###end###");
        System.out.println();

        try {
            System.out.println("Press Enter");
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("###DEMO Unirest###");
        demoService = new DemoUnirest();
        demoService.demo();
        System.out.println("###end###");
    }
}
